import { NextApiRequest, NextApiResponse } from "next";
import Timestamp from "timestamp";

export default function (req: NextApiRequest, res: NextApiResponse) {
  const DATE = Timestamp();
  res.json({
    unix: DATE.getTime(),
    utc: DATE.toUTCString(),
  });
}
