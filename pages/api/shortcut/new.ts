import { NextApiRequest, NextApiResponse } from "next";
import { promises as dns } from "dns";
import { Buffer } from "buffer";
import { REDIRECTS } from "../[shortcut]";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  try {
    const original_url = req.query.url;
    const valid_url = new URL(original_url.toString());

    await dns.lookup(valid_url.hostname);

    const find = await REDIRECTS.findOneAndUpdate(
      { original_url },
      { original_url },
      { upsert: true, returnNewDocument: true }
    );
    if (find) {
      const short_url = Buffer.from(find._id.toString(), "hex").toString(
        "base64"
      );
      res.json({ original_url, short_url });
    } else {
      throw Error;
    }
  } catch (error) {
    console.error(error);
    res.json({ error });
  }
}
