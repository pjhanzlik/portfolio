import {
  UserApiKeyCredential,
  Stitch,
  RemoteMongoClient,
} from "mongodb-stitch-server-sdk";

const APP_ID = "exercise_tracker_rest_api-ecgch";

const CLIENT = Stitch.hasAppClient(APP_ID)
  ? Stitch.getAppClient(APP_ID)
  : Stitch.initializeAppClient(APP_ID);

const credential = new UserApiKeyCredential(
  process.env.EXERCISE_TRACKER_REST_API as string
);

async () => {
  await CLIENT.auth.loginWithCredential(credential);
  await CLIENT.close();
};

const SERVICE_CLIENT = CLIENT.getServiceClient(
  RemoteMongoClient.factory,
  "mongodb-atlas"
);
const DATABASE = SERVICE_CLIENT.db("exercise-tracker");

export const USERS = DATABASE.collection<Exercise>("exercise");

export function exerciseCollection(username: string) {
  return DATABASE.collection<Exercise>(username);
}

export interface Exercise {}
