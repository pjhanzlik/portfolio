import { NextApiRequest, NextApiResponse } from "next";
import MetricImperialConverter from "metric-imperial-converter";

export default function (req: NextApiRequest, res: NextApiResponse) {
  try {
    res.json(MetricImperialConverter(req.query.input.toString()));
  } catch (error) {
    res.json(error);
  }
}
