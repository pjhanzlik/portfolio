import { NextApiRequest, NextApiResponse } from "next";
import {
  BSON,
  Stitch,
  UserApiKeyCredential,
  RemoteMongoClient,
} from "mongodb-stitch-server-sdk";
import { Buffer } from "buffer";

const APP_ID = "url_shortener_microservice-dbymu";
const CLIENT = Stitch.hasAppClient(APP_ID)
  ? Stitch.getAppClient(APP_ID)
  : Stitch.initializeAppClient(APP_ID);

const credential = new UserApiKeyCredential(
  process.env.URL_SHORTENER_MICROSERVICE as string
);

async () => {
  await CLIENT.auth.loginWithCredential(credential);
  await CLIENT.close();
};

export const REDIRECTS = CLIENT.getServiceClient(
  RemoteMongoClient.factory,
  "mongodb-atlas"
)
  .db("url-shortener-microservice")
  .collection<Redirect>("redirect");

export interface Redirect {
  original_url: string;
  _id: string[];
}

export default async function (req: NextApiRequest, res: NextApiResponse) {
  try {
    const _id = await new BSON.ObjectId(
      Buffer.from(req.query.shortcut.toString(), "base64").toString("hex")
    );
    const redirect = await REDIRECTS.findOne({ _id });
    if (redirect) {
      res.writeHead(301, {
        Location: redirect.original_url,
      });
      res.end();
    } else {
      throw Error;
    }
  } catch (error) {
    res.writeHead(307, {
      Location: `../../307/${req.query.shortcut.toString()}`,
    });
    res.end();
  } finally {
    CLIENT.close();
  }
}
