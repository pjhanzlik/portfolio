import { NextApiRequest, NextApiResponse } from "next";
import { generate } from "random-quote-machine";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  res.json(await generate());
}
