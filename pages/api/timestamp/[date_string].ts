import { NextApiRequest, NextApiResponse } from "next";
import Timestamp from "timestamp";

export default function (req: NextApiRequest, res: NextApiResponse) {
  const DATE = Timestamp(req.query.date_string.toString());
  res.json({
    unix: DATE.getTime(),
    utc: DATE.toUTCString(),
  });
}
