import Link from "next/link";

function FreeCodeCampNav() {
  return (
    <nav>
      <dl>
        <dt>Api and Microservices</dt>
        <dd>
          <Link href="/exercise-tracker">
            <a>
              <figure>
                <figcaption>Exercise Tracker</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/file-metadata">
            <a>
              <figure>
                <figcaption>File Metadata</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/request-header-parser">
            <a>
              <figure>
                <figcaption>Request Header Parser</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/timestamp">
            <a>
              <figure>
                <figcaption>Timestamp</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/url-shortener">
            <a>
              <figure>
                <figcaption>URL Shortener</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dt>Data Visualization</dt>
        <dd>
          <Link href="/bar-chart">
            <a>
              <figure>
                <figcaption>Bar Chart</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/choropleth-map">
            <a>
              <figure>
                <figcaption>Choropleth Map</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/heat-map">
            <a>
              <figure>
                <figcaption>Heat Map</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/scatterplot-graph">
            <a>
              <figure>
                <figcaption>Scatterplot Graph</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/treemap-diagram">
            <a>
              <figure>
                <figcaption>Treemap Diagram</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dt>Front End Libraries</dt>
        <dd>
          <Link href="/drum-machine">
            <a>
              <figure>
                <figcaption>Drum Machine</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/javascript-calculator">
            <a>
              <figure>
                <figcaption>Javascript Calculator</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/markdown-previewer">
            <a>
              <figure>
                <figcaption>Markdown Previewer</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/pomodoro-clock">
            <a>
              <figure>
                <figcaption>Pomodoro Clock</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/random-quote-machine">
            <a>
              <figure>
                <figcaption>Random Quote Machine</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dt>Information Security and Quality Assurance</dt>
        <dd>
          <Link href="/anonymous-message-board">
            <a>
              <figure>
                <figcaption>Anonymous Message Board</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/issue-tracker">
            <a>
              <figure>
                <figcaption>Issue Tracker</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/metric-imperial-converter">
            <a>
              <figure>
                <figcaption>Metric Imperial Converter</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/personal-library">
            <a>
              <figure>
                <figcaption>Personal Library</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/stock-price-checker">
            <a>
              <figure>
                <figcaption>Stock Price Checker</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dt>Responsive Web Design</dt>
        <dd>
          <Link href="/personal-portfolio-webpage">
            <a>
              <figure>
                <figcaption>Personal Portfolio Webpage</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/product-landing-page">
            <a>
              <figure>
                <figcaption>Product Landing Page</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/survey-form">
            <a>
              <figure>
                <figcaption>Survey Form</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/technical-landing-page">
            <a>
              <figure>
                <figcaption>Technical Landing Page</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
        <dd>
          <Link href="/tribute-page">
            <a>
              <figure>
                <figcaption>Tribute Page</figcaption>
              </figure>
            </a>
          </Link>
        </dd>
      </dl>
    </nav>
  );
}

export default function Index() {
  return <FreeCodeCampNav />;
}
