export default function () {
  return (
    <main>
      <h1>API Project: Timestamp Microservice</h1>
      <h2>User Stories (WIP):</h2>
      <ol>
        <li>
          The API endpoint is{" "}
          <code>GET [project_url]/api/timestamp/:date_string?</code>
        </li>
        <li>
          A date string is valid if can be successfully parsed by{" "}
          <code>new Date(date_string)</code>. Note that the unix timestamp needs
          to be an <strong>integer</strong> (not a string) specifying{" "}
          <strong>milliseconds</strong>. In our test we will use date strings
          compliant with ISO-8601 (e.g. "<code>2016-11-20</code>") because this
          will ensure an UTC timestamp.
        </li>
        <li>
          If the date string is <strong>empty</strong> it should be equivalent
          to trigger <code>new Date()</code>, i.e. the service uses the current
          timestamp.
        </li>
        <li>
          If the date string is valid the api returns a JSON having the
          structure{" "}
          <code>{`{"unix": <date.getTime()>, "utc" : <date.toUTCString()> }`}</code>{" "}
          e.g.{" "}
          <code>{`{"unix": 1479663089000 ,"utc": "Sun, 20 Nov 2016 17:31:29 GMT"}`}</code>
        </li>
        <li>
          If the date string is invalid the api returns a JSON having the
          structure
          <code>{`{"error" : "Invalid Date" }`}</code>.
        </li>
      </ol>
      <h2>Example Usage:</h2>
      <ul>
        <li>
          <a href="/api/timestamp/2015-12-25">
            [project url]/api/timestamp/2015-12-25
          </a>
        </li>
        <li>
          <a href="/api/timestamp/1450137600">
            [project url]/api/timestamp/1450137600
          </a>
        </li>
      </ul>
      <h2>Example Output:</h2>
      <code>{`{"unix":1451001600000, "utc":"Fri, 25 Dec 2015 00:00:00 GMT"}`}</code>
      <p>
        by <a href="https://www.freecodecamp.org/">freeCodeCamp</a>
      </p>
    </main>
  );
}
