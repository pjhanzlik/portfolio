import RandomQuoteMachine, { generate } from "random-quote-machine";
import { GetStaticProps } from "next";

export const getStaticProps: GetStaticProps = async (context) => {
  return { props: { quote: await generate() } };
};

export default RandomQuoteMachine;
