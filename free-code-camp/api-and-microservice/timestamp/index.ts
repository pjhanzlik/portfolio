export default function parse(date_string?: string): Date {
  if (date_string) {
    // 4 digits or more indicates Posix time
    return /^[\d]{4,}$/.test(date_string)
      ? new Date(parseInt(date_string))
      : new Date(date_string);
  } else {
    return new Date();
  }
}
