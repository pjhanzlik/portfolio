import { useReducer, useMemo } from "react";
import styles from "./components/QuoteBox.module.css";
import QuoteBox from "./components/QuoteBox";

export async function generate(): Promise<[string, Citation]> {
  return quotes[Math.floor(Math.random() * quotes.length)];
}

export interface Citation {
  readonly author?: string;
}

type Action = PendedQuote | FulfilledQuote | RejectedQuote;

export interface Quote {
  quote: [string, Citation];
}

interface PendedQuote {
  kind: "pended";
}

interface FulfilledQuote {
  kind: "fulfilled";
  quote: [string, Citation];
}

interface RejectedQuote {
  kind: "rejected";
  error: Error;
}

interface State {
  quote: [string, Citation];
  request: Promise<[string, Citation]>;
  hue: number;
}

function reducer(state: State, action: Action): State {
  switch (action.kind) {
    case "pended":
      return { ...state, request: generate() };
    case "fulfilled":
      return {
        ...state,
        quote: action.quote,
        hue: (Math.floor(Math.random() * 90) + state.hue + 90) % 361,
      };
    case "rejected":
      return state;
  }
}

export default function RandomQuoteMachine({ quote }: Quote) {
  const [state, dispatch] = useReducer(reducer, {
    quote,
    request: generate(),
    hue: Math.floor(Math.random() * 361),
  });

  useMemo(async () => {
    try {
      dispatch({ kind: "fulfilled", quote: await state.request });
    } catch (error) {
      dispatch({ kind: "rejected", error });
    }
  }, [state.request]);

  return (
    <QuoteBox quotation={state.quote[0]} citation={state.quote[1]}>
      {/* Temporarily disable style-jsx usage while enabling yarn zero-installs
                to prevent build node_module error
            <style global jsx>{`
				:root {
					--primary-color: hsl(${state.hue},40%,40%);
					--secondary-color: whitesmoke
				}
			`}</style> */}
      <button
        className={styles.button}
        onClick={() => dispatch({ kind: "pended" })}
      >
        {"New quote"}
      </button>
    </QuoteBox>
  );
}

// Public domain from TheGoldenQuotes.Net funny section.
const quotes: Array<[string, Citation]> = [
  [
    "When I was born, the doctor came out to the waiting room and said to my father.. I’m very sorry, We did everything we could.. But he pulled through…",
    { author: "Rodney Dangerfield" },
  ],
  [
    "Patience is something you admire in the driver behind you, but not in one ahead…",
    { author: "Bill McGlashen" },
  ],
  [
    "A computer once beat me at chess, but it was no match for me at kick boxing…",
    { author: "Emo Philips" },
  ],
  [
    "I’m so clever that sometimes I don’t understand a single word of what I’m saying…",
    { author: "Oscar Wilde" },
  ],
  [
    "You have to stay in shape. My grandmother, she started walking five miles a day when she was 60. She’s 97 today and we don’t know where the hell she is…",
    { author: "Ellen DeGeneres" },
  ],
  [
    "If you die in an elevator, be sure to push the up button…",
    { author: "Sam Levenson" },
  ],
  [
    "If you think nobody cares if you’re alive, try missing a couple of car payments…",
    { author: "Earl Wilson" },
  ],
  [
    "The broccoli says: I look like a small tree , the mushroom says: I look like an umbrella, the walnut says: I look like a brain, and the banana says: Can We Please Change The Subject?…",
    {},
  ],
  [
    "Gravitation is not responsible for people falling in love…",
    { author: "Albert Einstein" },
  ],
  ["I didn’t fall. The floor just needed a hug…", {}],
  [
    "The quickest way to double your money is to fold it over and put it back in your pocket…",
    { author: "Will Rogers" },
  ],
  [
    "Always borrow money from a pessimist. He won’t expect it back…",
    { author: "Oscar Wilde" },
  ],
  [
    "Better to remain silent and be thought a fool than to speak out and remove all doubt…",
    { author: "Abraham Lincoln" },
  ],
  [
    "They say that love is more important than money, but have you ever tried to pay your bills with a hug?…",
    {},
  ],
  [
    "I couldn’t repair your brakes, so I made your horn louder…",
    { author: "Steven Wright" },
  ],
  [
    "Before criticizing someone, walk a mile in their shoes. Then when you do criticize them, you will be a mile away and have their shoes…",
    { author: "Jack Handey" },
  ],
  [
    "If you think you are too small to be effective, you have never been in the dark with a mosquito…",
    { author: "Betty Reese" },
  ],
  [
    "A bank is a place that will lend you money if you can prove that you don’t need it…",
    { author: "Bob Hope" },
  ],
  [
    "By the time a man realizes that maybe his father was right, he usually has a son who thinks he’s wrong…",
    { author: "Charles Wadsworth" },
  ],
  [
    "It takes 8,460 bolts to assemble an automobile, and one nut to scatter it all over the road…",
    { author: "Yogi Berra" },
  ],
  [
    "I have enough money to last me the rest of my life, unless I buy something…",
    { author: "Jackie Mason" },
  ],
  [
    "Did you ever walk into a room and forget why you walked in? I think that’s how dogs spend their lives…",
    { author: "Sue Murphy" },
  ],
  [
    "Evening news is where they begin with ‘Good evening’, and then proceed to tell you why it isn’t…",
    {},
  ],
  ["Good Enough is the New Perfect…", { author: "Becky Beaupre Gillespie" }],
  [
    "Television has proved that people will look at anything rather than each other…",
    { author: "Ann Landers" },
  ],
  [
    "The human body was designed by a civil engineer. Who else would run a toxic waste pipeline through a recreational area?…",
    { author: "Robin Williams" },
  ],
  [
    "We must believe in luck. For how else can we explain the success of those we don’t like?…",
    { author: "Jean Cocturan" },
  ],
  [
    "I am not a vegetarian because I love animals; I am a vegetarian because I hate plants…",
    { author: "Whitney Brown" },
  ],
  [
    "Never go to a doctor whose office plants have died…",
    { author: "Erma Bombeck" },
  ],
  [
    "We all pay for life with death, so everything in between should be free…",
    { author: "Bill Hicks" },
  ],
  [
    "I have a simple philosophy. Fill what’s empty. Empty what’s full. And scratch where it itches…",
    { author: "Alice Roosevelt Longworth" },
  ],
  [
    "People say dreams can come true, but they don’t tell you nightmares can too…",
    { author: "Silvestre Lopez" },
  ],
  [
    "Never pass up the opportunity to have sex or appear on television…",
    { author: "Gore Vidal" },
  ],
  [
    "Everybody knows how to raise children, except the people who have them…",
    { author: "J. O'Rourke" },
  ],
  [
    "A pessimist is someone who looks at the land of milk and honey and sees only calories and cholesterol…",
    { author: "Dylan Wimberley" },
  ],
  [
    "To avoid criticism, do nothing, say nothing, be nothing…",
    { author: "Elbert Hubbard" },
  ],
  [
    "To be good is noble, but to teach others how to be good is nobler and less trouble…",
    { author: "Mark Twain" },
  ],
  ["Love your enemies. It makes them so damned mad…", { author: "P. D. East" }],
  [
    "If a man could live forever, he would still be a fish. Why would he evolve?.. He could always do it later…",
    { author: "Aki Soga" },
  ],
  [
    "I don’t like to commit myself about heaven and hell.. You see, I have friends in both places…",
    { author: "Mark Twain" },
  ],
  [
    "A celebrity is a person who works hard all of their life to become well known, and then wears dark glasses to avoid being recognized…",
    { author: "Fred Allen" },
  ],
  [
    "Happiness is having a large, loving, caring, close-knit family in another city…",
    { author: "George Burns" },
  ],
  [
    "There are two kinds of people who don't say much, those who are quiet and those who talk a lot…",
    {},
  ],
  [
    "Hollywood is a place where they’ll pay you a thousand dollars for a kiss and fifty cents for your soul…",
    { author: "Marilyn Monroe" },
  ],
  [
    "True love is like ghosts, which everybody talks about and few have seen…",
    { author: "Francois De La Rochefoucauld" },
  ],
  ["Never take life seriously. Nobody gets out alive anyway…", {}],
  [
    "Before I got married I had six theories about bringing up children; now I have six children and no theories…",
    { author: "John Wilmot" },
  ],
  [
    "Retirement at sixty-five is ridiculous. When I was sixty-five, I still had pimples…",
    { author: "George Burns" },
  ],
  [
    "Everything has been figured out, except how to live…",
    { author: "Jean-Paul Sartre" },
  ],
  [
    "The trouble with the rat race is that even if you win, you’re still a rat…",
    { author: "Lily Tomlin" },
  ],
  [
    "The average dog is a nicer person than the average person…",
    { author: "Andy Rooney" },
  ],
  [
    "Go to Heaven for the climate, Hell for the company…",
    { author: "Mark Twain" },
  ],
  [
    "People say that money is not the key to happiness, but I always figured if you have enough money, you can have a key made…",
    { author: "Joan Rivers" },
  ],
  [
    "I don’t think inside the box, I don’t think outside the box, I don’t even know where the box is!…",
    { author: "Scott Douglas Chase" },
  ],
  [
    "I never drink water because of the disgusting things that fish do in it…",
    { author: "W. C. Fields" },
  ],
  [
    "Life is a tragedy when seen in close-up, but a comedy in long-shot…",
    { author: "Charlie Chaplin" },
  ],
  [
    "All you need in this life is ignorance and confidence; then success is sure…",
    { author: "Mark Twain" },
  ],
  [
    "If you want to make God laugh, tell him about your plans…",
    { author: "Woody Allen" },
  ],
  [
    "When we remember we are all mad, the mysteries disappear and life stands explained…",
    { author: "Mark Twain" },
  ],
  [
    "I just found human hairs in my McDonald’s burger. When did they start using natural ingredients?…",
    {},
  ],
  [
    "Before I met my husband, I’d never fallen in love. I’d stepped in it a few times…",
    { author: "Rita Rudner" },
  ],
  [
    "There are three kinds of men. The one that learns by reading. The few who learn by observation. The rest of them have to pee on the electric fence for themselves…",
    { author: "Will Rogers" },
  ],
  [
    "Knowledge is realizing that the street is one-way, wisdom is looking both directions anyway…",
    { author: "Amelia Walker" },
  ],
  ["No one likes change but babies in diapers…", { author: "Barbara Johnson" }],
  [
    "Life is just one damned thing after another…",
    { author: "Elbert Hubbard" },
  ],
  [
    "We are all here on earth to help others; what on earth the others are here for I don't know…",
    { author: "W. H. Auden" },
  ],
  [
    "Everything is funny, as long as it's happening to somebody else…",
    { author: "Will Rogers" },
  ],
  [
    "Behind every great man is a woman rolling her eyes…",
    { author: "Jim Carry" },
  ],
  [
    "Progress is man's ability to complicate simplicity…",
    { author: "Thor Heyerdahl" },
  ],
  [
    "Don't talk about yourself; it will be done when you leave…",
    { author: "Wilson Mizner" },
  ],
  [
    "Why do they call it rush hour when nothing moves?…",
    { author: "Robin Williams" },
  ],
  [
    "There is little chance that meteorologists can solve the mysteries of weather until they gain an understanding of the mutual attraction of rain and weekends…",
    { author: "Arnot Sheppard" },
  ],
  [
    "The best way to get a puppy is to beg for a baby brother – and they’ll settle for a puppy every time…",
    { author: "Winston Pendelton" },
  ],
  ["Weather forecast for tonight: Dark…", { author: "George Carlin" }],
  [
    "Laziness is nothing more than the habit of resting before you get tired…",
    { author: "Jules Renard" },
  ],
  [
    "Always remember that you are absolutely unique. Just like everyone else…",
    { author: "Margaret Mead" },
  ],
  [
    "No man has a good enough memory to be a successful liar…",
    { author: "Abraham Lincoln" },
  ],
  [
    "Age is something that doesn't matter, unless you are a cheese…",
    { author: "Luis Bunuel" },
  ],
  [
    "When you are courting a nice girl an hour seems like a second. When you sit on a red-hot cinder a second seems like an hour. That's relativity…",
    { author: "Albert Einstein" },
  ],
  [
    "The day after tomorrow is the third day of the rest of your life…",
    { author: "George Carlin" },
  ],
  [
    "Life is something to do when you can’t get to sleep…",
    { author: "Fran Lebowitz" },
  ],
  [
    "I can win an argument on any topic, against any opponent. People know this, and steer clear of me at parties. Often, as a sign of their great respect, they don’t even invite me…",
    { author: "Dave Barry" },
  ],
  [
    "When a man’s best friend is his dog, that dog has a problem…",
    { author: "Edward Abbey" },
  ],
  [
    "Wine is constant proof that God loves us and loves to see us happy…",
    { author: "Benjamin Franklin" },
  ],
  [
    "To succeed in life, you need three things: a wishbone, a backbone and a funny bone…",
    { author: "Reba McEntire" },
  ],
  [
    "A day without sunshine is like, you know, night…",
    { author: "Steve Martin" },
  ],
  [
    "Laugh and the world laughs with you, snore and you sleep alone…",
    { author: "Anthony Burgess" },
  ],
];
