import { Citation } from "../index";
import { ReactNode } from "react";
import styles from "./QuoteBox.module.css";

interface Props {
  quotation: string;
  citation: Citation;
  children?: ReactNode;
}

export default function QuoteBox(props: Props) {
  return (
    <div className={styles.body}>
      <div className={styles.main}>
        <blockquote className={styles.blockquote}>
          <p className={styles.p}>{props.quotation}</p>
          <cite className={styles.cite}>{props.citation.author}</cite>
        </blockquote>
        <div className={styles.links}>
          <a
            className={styles.a}
            href={`https://twitter.com/intent/tweet?text=${
              props.quotation
            }&hashtags=${
              props.citation.author
                ? props.citation.author.replace(/\W/g, "")
                : ""
            }`}
          >
            Tweet quote
          </a>
        </div>
        <div className={styles.children}>{props.children}</div>
      </div>
    </div>
  );
}
