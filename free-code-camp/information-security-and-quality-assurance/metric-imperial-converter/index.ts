type Mass = "pound" | "gram";
type Length = "meter" | "mile";
type Volume = "gallon" | "liter";
type Prefix =
  | "yocto"
  | "zepto"
  | "atto"
  | "femto"
  | "pico"
  | "nano"
  | "micro"
  | "milli"
  | "centi"
  | "deci"
  | "deca"
  | "hecto"
  | "kilo"
  | "mega"
  | "giga"
  | "tera"
  | "peta"
  | "exa"
  | "zetta"
  | "yotta";
interface Metric {
  base: "liter" | "gram" | "meter";
  prefix?: Prefix;
}
type Imperial = "gallon" | "pound" | "mile";
type ScientificUnit =
  | "meter"
  | { base: "gram"; prefix: "kilo" }
  | "liter"
  | Imperial;
type BaseUnit = "meter" | "gram" | "liter" | Imperial;
type WesternUnit = Metric | Imperial;
type Measurement = {
  numerator?: number;
  denominator?: number;
  unit: Metric | { base: Imperial };
};
type ResJSON = {
  initNum: number;
  initUnit: WesternUnit;
  returnNum: number;
  returnUnit: ScientificUnit;
  string: string;
};

function parseBaseUnit(baseUnitAlias: string): BaseUnit {
  switch (baseUnitAlias.toLocaleLowerCase().replace(/s$/, "")) {
    case "gallon":
    case "gal":
      return "gallon";
    case "mile":
      return "mile";
    case "pound":
    case "lb":
      return "pound";
    case "liter":
      return "liter";
    case "meter":
      return "meter";
    case "gram":
      return "gram";
    default:
      switch (baseUnitAlias.toLocaleLowerCase()) {
        case "mi":
          return "mile";
        case "l":
          return "liter";
        case "m":
          return "meter";
        case "g":
          return "gram";
        default:
          throw new Error(
            `Could not parse base unit name from ${baseUnitAlias}`
          );
      }
  }
}

function parsePrefix(prefixAlias: string): Prefix {
  switch (prefixAlias) {
    case "y":
      return "yocto";
    case "z":
      return "zepto";
    case "a":
      return "atto";
    case "f":
      return "femto";
    case "p":
      return "pico";
    case "n":
      return "nano";
    case "µ":
    case "mu":
      return "micro";
    case "m":
      return "milli";
    case "c":
      return "centi";
    case "d":
      return "deci";
    case "da":
    case "㍲":
      return "deca";
    case "h":
      return "hecto";
    case "k":
      return "kilo";
    case "M":
      return "mega";
    case "G":
      return "giga";
    case "T":
      return "tera";
    case "P":
      return "peta";
    case "E":
      return "exa";
    case "Z":
      return "zetta";
    case "Y":
      return "yotta";
    default:
      switch (prefixAlias.toLocaleLowerCase()) {
        case "yocto":
          return "yocto";
        case "zepto":
          return "zepto";
        case "atto":
          return "atto";
        case "femto":
          return "femto";
        case "pico":
          return "pico";
        case "nano":
          return "nano";
        case "micro":
          return "micro";
        case "milli":
          return "milli";
        case "centi":
          return "centi";
        case "deci":
          return "deci";
        case "deca":
          return "deca";
        case "hecto":
          return "hecto";
        case "kilo":
          return "kilo";
        case "mega":
          return "mega";
        case "giga":
          return "giga";
        case "tera":
          return "tera";
        case "peta":
          return "peta";
        case "exa":
          return "exa";
        case "zetta":
          return "zetta";
        case "yotta":
          return "yotta";
        default:
          throw Error(`Could not parse prefix from ${prefixAlias}`);
      }
  }
}

export default function (input: string): ResJSON {
  const EXPRESSED = {
    groups: {
      numerator: "1.1",
      denominator: "1.2",
      prefix: "k",
      base: "g",
    },
  };

  const baseUnit = parseBaseUnit(EXPRESSED.groups.base);
  if (baseUnit) {
  }

  const measurement: Measurement = {
    numerator: Number.parseFloat(EXPRESSED.groups.numerator),
    denominator: Number.parseFloat(EXPRESSED.groups.denominator),
    unit: {
      prefix: parsePrefix(EXPRESSED.groups.prefix),
      base: parseBaseUnit(EXPRESSED.groups.base),
    },
  };
  return {
    initNum: 404,
    initUnit: "gallon",
    returnNum: 404,
    returnUnit: "gallon",
    string: "",
  };
}
