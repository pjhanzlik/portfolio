import Head from "next/head";
import styles from "./Index.module.css";

export default function Index() {
  return (
    <article className={styles.article}>
      <Head>
        <title className={styles.title}>FCC Survey Form</title>
      </Head>
      <h1 className={styles.h1} id="title">
        Survey Form
      </h1>
      <main className={styles.main}>
        <p className={styles.p} id="description">
          Please fill out this unofficial form to let freeCodeCamp know how to
          improve their teaching system.
        </p>
        <form className={styles.form} id="survey-form">
          <div className={styles.div}>
            <input
              className={styles.input}
              type="text"
              name="name"
              id="name"
              required
              placeholder="Enter your name"
            />
            <label className={styles.label} htmlFor="name" id="name-label">
              Preferred Name
            </label>
          </div>
          <div className={styles.div}>
            <input
              className={styles.input}
              type="email"
              name="email"
              id="email"
              required
              placeholder="Enter your email"
            />
            <label className={styles.label} htmlFor="email" id="email-label">
              Contact Email
            </label>
          </div>
          <div className={styles.div}>
            <input
              className={styles.input}
              type="number"
              name="number"
              id="number"
              min="0"
              max="24"
              placeholder="24"
            />
            <label className={styles.label} htmlFor="number" id="number-label">
              Daily Hours Spent Coding
            </label>
          </div>
          <div className={styles.div}>
            <select className={styles.select} name="dropdown" id="dropdown">
              <option className={styles.option} selected>
                I love them all equally
              </option>
              <option className={styles.option}>
                Responsive Web Design Certification
              </option>
              <option className={styles.option}>
                Javascript Algorithms And Data Structures Certification
              </option>
              <option className={styles.option}>
                Front End Libraries Certification
              </option>
              <option className={styles.option}>
                Data Visualization Certification
              </option>
              <option className={styles.option}>
                Apis And Microservices Certification
              </option>
              <option className={styles.option}>
                Information Security And Quality Assurance Certification
              </option>
              <option className={styles.option}>Coding Interview Prep</option>
            </select>
            <label
              className={styles.label}
              htmlFor="dropdown"
              id="dropdown-label"
            >
              Favorite FCC Lesson Plan
            </label>
          </div>
          <div className={styles.div}>
            <label className={styles.label} htmlFor="priority">
              How helpful was FCC to you?
            </label>
            <div className={styles.div}>
              <input
                className={styles.input}
                name="priority"
                id="showstopper"
                value="showstopper"
                type="radio"
              />
              <label
                className={styles.label}
                htmlFor="showstopper"
                id="showstopper-label"
              >
                Showstopper
              </label>
              <input
                className={styles.input}
                name="priority"
                id="high"
                value="high"
                type="radio"
              />
              <label className={styles.label} htmlFor="high" id="high-label">
                High
              </label>
              <input
                className={styles.input}
                name="priority"
                id="medium"
                value="medium"
                type="radio"
              />
              <label
                className={styles.label}
                htmlFor="medium"
                id="medium-label"
              >
                Medium
              </label>
              <input
                className={styles.input}
                name="priority"
                id="low"
                value="low"
                type="radio"
              />
              <label className={styles.label} htmlFor="low" id="low-label">
                Low
              </label>
              <input
                className={styles.input}
                name="priority"
                id="cosmetic"
                value="cosmetic"
                type="radio"
              />
              <label
                className={styles.label}
                htmlFor="cosmetic"
                id="cosmetic-label"
              >
                Cosmetic
              </label>
              <input
                className={styles.input}
                name="priority"
                id="unsure"
                value="unsure"
                type="radio"
                defaultChecked
              />
              <label
                className={styles.label}
                htmlFor="unsure"
                id="unsure-label"
              >
                Unsure
              </label>
            </div>
          </div>
          <div className={styles.div}>
            <label className={styles.label} htmlFor="interests">
              What are your development interests?
            </label>
            <div className={styles.div}>
              <input
                className={styles.input}
                type="checkbox"
                name="interests"
                id="embedded"
                value="embedded"
              />
              <label
                className={styles.label}
                htmlFor="embedded"
                id="embedded-label"
              >
                Embedded
              </label>
              <input
                className={styles.input}
                type="checkbox"
                name="interests"
                id="web"
                value="web"
              />
              <label className={styles.label} htmlFor="web" id="web-label">
                Web
              </label>
              <input
                className={styles.input}
                type="checkbox"
                name="interests"
                id="os"
                value="os"
              />
              <label className={styles.label} htmlFor="os" id="os-label">
                Operating System
              </label>
              <input
                className={styles.input}
                type="checkbox"
                name="interests"
                id="cpu"
                value="cpu"
              />
              <label className={styles.label} htmlFor="cpu" id="cpu-label">
                CPU
              </label>
              <input
                className={styles.input}
                type="checkbox"
                name="interests"
                id="graphics"
                value="graphics"
              />
              <label
                className={styles.label}
                htmlFor="graphics"
                id="graphics-label"
              >
                Graphics
              </label>
              <input
                className={styles.input}
                type="checkbox"
                name="interests"
                id="game"
                value="game"
              />
              <label className={styles.label} htmlFor="game" id="game-label">
                Game
              </label>
            </div>
          </div>
          <div className={styles.div}>
            <textarea
              className={styles.textarea}
              name="comments"
              id="comments"
              placeholder="Tell us anything!"
            ></textarea>
            <label
              className={styles.label}
              htmlFor="comments"
              id="comments-label"
            >
              Additional Comments
            </label>
          </div>
          <button className={styles.button} type="submit" id="submit">
            Submit
          </button>
        </form>
      </main>
    </article>
  );
}
