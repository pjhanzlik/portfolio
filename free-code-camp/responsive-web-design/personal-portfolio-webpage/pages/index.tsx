import styles from "./Index.module.css";
import Head from "next/head";
import Link from "next/link";

// ProjectTile might greatly benefit from using the POW format https://css-tricks.com/pow/
// or it could also use SVG to minimize size and resizing issues
function ProjectTile(props: {
  href: string;
  name: string;
  screenshot?: string;
}) {
  return (
    <Link href={props.href}>
      <a>
        <figure className="project-tile">
          {props.screenshot && (
            <img
              height="390"
              width="400"
              src={props.screenshot}
              alt={`Screenshot of ${props.name}`}
            />
          )}
          <figcaption>{props.name}</figcaption>
        </figure>
      </a>
    </Link>
  );
}

export default function Index() {
  return (
    <article className={styles.body}>
      <Head>
        <title>Paul Hanzlik Personal Portfolio</title>
      </Head>
      <header id="navbar">
        <a href="#welcome-section">About</a>
        <a href="#projects">Work</a>
        <a href="#contact">Contact</a>
      </header>
      <div>
        <main>
          <header id="welcome-section">
            <h1>Hi, I'm Paul!</h1>
            <p>a computer programmer</p>
          </header>
          <section id="projects">
            <h2>My freeCodeCamp Projects</h2>
            <div>
              <ProjectTile
                href="https://pjhanzlik.gitlab.io/tribute-page"
                name="Tribute Page"
              />
              <ProjectTile
                href="https://pjhanzlik.gitlab.io/survey-form"
                name="Survey Form"
              />
              <ProjectTile
                href="https://pjhanzlik.gitlab.io/product-landing-page"
                name="Product Landing Page"
              />
              <ProjectTile
                href="https://pjhanzlik.gitlab.io/technical-documentation-page"
                name="Technical Documentation Page"
              />
            </div>
            <a
              id="profile-link"
              href="https://www.freecodecamp.org/pjhanzlik"
              target="_blank"
            >
              Show all ➡️
            </a>
          </section>
          <footer id="contact">
            <h2>Let's work together...</h2>
            <p>How do you take your coffee?</p>
            <nav>
              <a href="https://gitlab.com/pjhanzlik">📟 GitLab</a>
              <a href="mailto:pjhanzlik@gmail.com">📧️ Email</a>
              <a href="tel:763-245-2632">📱️ Phone</a>
            </nav>
          </footer>
        </main>
        <footer>
          <p>
            This portfolio was created for freeCodeCamp's personal porfolio
            project.
          </p>
        </footer>
      </div>
    </article>
  );
}
