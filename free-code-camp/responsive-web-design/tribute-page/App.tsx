function Portrait(props: {
  title?: string;
  alt: string;
  src: URL;
  figcaption: string;
}) {
  return (
    <figure>
      <img title={props.title} alt={props.alt} src={props.src.toString()} />
      <figcaption>{props.figcaption}</figcaption>
    </figure>
  );
}

function Event(props: { event: [Date, string] }) {
  return (
    <>
      <dt>
        <time>{props.event[0].getFullYear()}</time>
      </dt>
      <dd>{props.event[1]}</dd>
    </>
  );
}
function Timeline(props: { subject: string; events: [Date, string][] }) {
  const events = props.events.map((event) => (
    <Event event={event} key={event.toString()} />
  ));
  return (
    <>
      <h2>{props.subject}</h2>
      <dl>{events}</dl>
    </>
  );
}

export interface Props {
  heading: string;
  lead: string;
  portrait: {
    img: {
      alt: string;
      src: URL;
    };
    contentinfo: string;
  };
  timeline: {
    heading: string;
    events: [Date, string][];
  };
}

export default function App() {
  return (
    <article>
      <h1>Spork</h1>
      <p>The Bidoof who saved a party</p>
      <Portrait
        alt="Screenshot of a Pokemon Go Bidoof in shaggy green grass."
        title='"Bidoof wants to be yr frendo. 9245 4702 3990 is me - if you play, feel free to, as the kids say, hit me up." by john_pittman is licensed under CC BY-NC-SA 2.0'
        src={
          new URL(
            "https://live.staticflickr.com/65535/48147808606_24c20730b6.jpg"
          )
        }
        figcaption="Spork, center stage, awaits capture in some tall grass - he's just along for the ride."
      />
      <Timeline
        subject="Here's a time line of Spork's life:"
        events={[
          [new Date(2019, 1), "Caught in Route 201"],
          [
            new Date(2019, 1),
            "Leaves his Poké Ball to level up, thanks to a nervous trainer known as JoCat",
          ],
          [
            new Date(2019, 1),
            "Learns the move Rock Smash to smash rocks in Route 207",
          ],
          [
            new Date(2019, 1),
            "Has to stop training to let SingulariT in the party. Hangs out in Box 1 which makes JoCat a little sad.",
          ],
          [
            new Date(2019, 1),
            `"I'm actually about to cry", he said. "I'll be honest."`,
          ],
          [
            new Date(2019, 1),
            "Receives praise for being having Lax nature and often dozing off",
          ],
          [
            new Date(2019, 1),
            `JoCat states "we don't want to use Spork, because Spork will just make this easy mode." He proclaims "we know Spork can just snap this entire gym."`,
          ],
          [new Date(2019, 1), "dies at the level of 4."],
        ]}
      />
      <blockquote cite="https://www.youtube.com/watch?v=VC7lRZTxDng&feature=youtu.be&t=1408">
        Luckily it was not a Pokémon that we needed. It's just a Pokémon that
        makes things a lot better. Bidoof makes everything better when you play.
        Now everything just sucks.
        <footer>Pokémon Trainer Joseph Catalanello</footer>
      </blockquote>
      <p>
        If you have time, you should witness this incredible animal sacrifice
        himself during{" "}
        <a href="https://youtu.be/VC7lRZTxDng?t=1309">
          JoCat's second Nuzlocke video
        </a>
      </p>
    </article>
  );
}
