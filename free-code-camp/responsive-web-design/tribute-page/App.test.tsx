import Index, { Props } from "./App";

const props: Props = {
  heading: "An example Tribute Page about nothing.",
  lead: "This is a test of the tribute page component",
  portrait: {
    img: {
      alt: "A picture of El Capitan",
      src: new URL("todo"),
    },
    contentinfo: "CAPTAIN CAPTION",
  },
  timeline: {
    heading: "Here is the life of El Capitan",
    events: [[new Date(2019, 1), ""]],
  },
};

test.todo(
  "`props.title: string` is text within the first heading in the only article"
);

test.todo("`props.portrait.img: URL` ");

test.todo(
  `Within the only article is a figure containing text "Spork, center stage, awaits capture in some tall grass - he's just along for the ride."`
);

test.todo(
  "Within the only article is a list containing items with time-text children"
);

test.todo("Within the only article is a blockquote");

test.todo("<TributePage blockquote /> contains `<Blockquote ...blockquote />`");

test.todo("<TributePrompt prompt /> contains `<Prompt ...prompt />`");
