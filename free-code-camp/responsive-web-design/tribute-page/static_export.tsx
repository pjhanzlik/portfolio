import App, { Props } from "./App";
import ReactDOMServer from "react-dom/server";

const defaultProps: Props = {
  heading: "An example Tribute Page about nothing.",
  lead: "This is a test of the tribute page component",
  portrait: {
    img: {
      alt: "A picture of El Capitan",
      src: new URL("todo"),
    },
    contentinfo: "CAPTAIN CAPTION",
  },
  timeline: {
    heading: "Here is the life of El Capitan",
    events: [[new Date(2019, 1), ""]],
  },
};

export function render(req: Props = defaultProps, res) {
  const html = ReactDOMServer.renderToString(<App {...req} />);
  res.json({ html });
}
