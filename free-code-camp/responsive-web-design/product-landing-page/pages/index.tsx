import styles from "./Index.module.css";
import Head from "next/head";
import Link from "next/link";

function NavLink(props: { targetID: string; targetName?: string }) {
  return (
    <Link href={`#${props.targetID}`}>
      <a className="nav-link">
        {props.targetName ? props.targetName : props.targetID}
      </a>
    </Link>
  );
}

export default function Index() {
  return (
    <article className={styles.body}>
      <Head>Velocipede Squeakers</Head>
      <header id="header">
        <span>
          <img
            height="30px"
            id="header-img"
            src="https://svgsilh.com/svg/1295038.svg"
            alt="Velocipede Squeakers logo."
            title="SVG Silh: These SVG images were created by modifying the images of Pixabay. These are released under Creative Commons CC0. Copyright svgsilh.com all rights reserved."
          />
          <h1>Velocipede Squeakers</h1>
        </span>
        <nav id="nav-bar">
          <NavLink targetID="top" targetName="Benefits" />
          <NavLink targetID="video" targetName="Demonstration" />
          <NavLink targetID="products" targetName="Products" />
        </nav>
      </header>
      <main id="top">
        <article>
          <h2>Machined, mass-produced treasures</h2>
          <form id="form" action="https://www.freecodecamp.com/email-submit">
            <input
              type="email"
              name="email"
              id="email"
              placeholder="Insert email address here"
            />
            <input
              type="submit"
              id="submit"
              value="Get Informed"
              disabled
            ></input>
          </form>
          <section>
            <div>
              <span>✊</span>
              <h2>Outstanding Durability</h2>
              <p>
                Built to take a tumble, our horns produce some of the squeakiest
                sounds around in all weather conditions.
              </p>
            </div>
            <div>
              <span>✋</span>
              <h2>Delivers Fast</h2>
              <p>
                One quick squeeze puts you right in the center of attention. Oh,
                and all horns always ship first class.
              </p>
            </div>
            <div>
              <span>👍</span>
              <h2>Quality Guaranteed</h2>
              <p>
                Should our product give up the ghost before you do we will
                gladly ship you another for free!
              </p>
            </div>
          </section>
          <video id="video" controls preload="metadata">
            <source
              src="https://archive.org/download/LMCC_Safety_Expert_-_Trail_Safety/LMCC_Safety_Expert_-_Trail_Safety.mp4"
              type="video/mp4"
            />
            <source
              src="https://archive.org/download/LMCC_Safety_Expert_-_Trail_Safety/LMCC_Safety_Expert_-_Trail_Safety.ogv"
              type="video/ogg"
            />
          </video>
        </article>
        <ul id="products">
          <li>
            <h3>Cute Horn</h3>
            <ul>
              <li>
                <strong>$3</strong>
              </li>
              <li>Dolor sit.</li>
              <li>Dolor sit.</li>
              <li>Dolor sit amet.</li>
              <li>Dolor sit.</li>
            </ul>
            <button>Select</button>
          </li>
          <li>
            <h3>Cool Horn</h3>
            <ul>
              <li>
                <strong>$6</strong>
              </li>
              <li>Dolor sit.</li>
              <li>Dolor sit.</li>
              <li>Dolor sit amet.</li>
              <li>Dolor sit.</li>
            </ul>
            <button>Select</button>
          </li>
          <li>
            <h3>Best Horn</h3>
            <ul>
              <li>
                <strong>$12</strong>
              </li>
              <li>Wonderful for beginners</li>
              <li>Easy to hold and mount</li>
              <li>Dolor sit amet.</li>
              <li>Dolor sit.</li>
            </ul>
            <button>Select</button>
          </li>
        </ul>
        <footer id="references">
          <nav>
            <a href="#top">Top</a>
            <a href="#video">Terms</a>
            <a href="#form">Contact</a>
          </nav>
          <p>Copyright 2019, Velocipede Squeakers</p>
        </footer>
      </main>
    </article>
  );
}
